==================================================
Welcome to the documentation of Lg Python SDK!
==================================================
.. |br| raw:: html

   <div style="line-height: 0; padding: 0; margin: 0"></div>

.. toctree::
   :maxdepth: 2

Billing
------------------------------------------------------
.. automodule:: lg_pack.service.billing
    :members:

Case
-------------------------------------------------------
.. automodule:: lg_pack.service.case
    :members:

Document
-------------------------------------------------------
.. automodule:: lg_pack.service.document
    :members:

Task/Event
--------------------------------------------------------
.. automodule:: lg_pack.service.task_event
    :members:

Lead Generation
--------------------------------------------------------
.. automodule:: lg_pack.service.lead
    :members:

Database Query
--------------------------------------------------------
.. automodule:: lg_pack.db_service.db_case
    :members:
