import json
from lg_pack.base.db_service import DBService


class DBCase(DBService):
    @classmethod
    def get_data_by_case_id(cls, case_id, json_format=False):
        """
        Get all the data of a given CaseID from CRM.

        Parameters
        ----------
        case_id : integer
            CaseID of the User of the CRM.

        json_format : bool, optional
            If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary.

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter
        """
        query_string = 'SELECT * FROM public.core_case WHERE case_id={case_id};'.format(
            case_id=case_id
        )
        query_result = DBService()._run_query(
            query=query_string
        )
        if query_result:
            result_dict = {}
            for _inc, columns in enumerate(query_result['table_column_list']):
                result_dict[str(columns.name)] = query_result['table_data_list'][0][_inc]
            response_dict = {
                "status": "Successfully queried a Case",
                "data": result_dict
            }
        else:
            response_dict = {
                "status": "Failed to query a Case. Please check your CaseID"
            }

        if json_format:
            response = json.dumps(response_dict)
        else:
            response = response_dict

        return response
