import psycopg2
from lg_pack.base.ini_file_service import _parse_ini_file

DB_NAME = _parse_ini_file(section='DB_CONFIGS', key='DB_NAME')
USER_NAME = _parse_ini_file(section='DB_CONFIGS', key='USER_NAME')
USER_PASSWORD = _parse_ini_file(section='DB_CONFIGS', key='USER_PASSWORD')
HOST = _parse_ini_file(section='DB_CONFIGS', key='HOST')
PORT = _parse_ini_file(section='DB_CONFIGS', key='PORT')


class DBService(object):
    """
    Base Service for Remote Database connection
    """
    def __init__(self):
        if DB_NAME:
            self.DB_NAME = DB_NAME
        else:
            raise ValueError("Database Name is missing in the Configuration File")

        if USER_NAME:
            self.USER_NAME = USER_NAME
        else:
            raise ValueError("Database User Name is missing in the Configuration File")

        if USER_PASSWORD:
            self.USER_PASSWORD = USER_PASSWORD
        else:
            raise ValueError("Database User Password is missing in the Configuration File")

        if HOST:
            self.HOST = HOST
        else:
            raise ValueError("Database Host is missing in the Configuration File")

        if PORT:
            self.PORT = PORT
        else:
            raise ValueError("Database Port is missing in the Configuration File")

    def __setup_db(self, query):
        """
        Initialize Database settings
        :param query: Query string to execute
        :return: Query Results
        """
        connection = False
        table_column_name = None
        result = None
        try:
            connection = psycopg2.connect(user=self.USER_NAME,
                                          password=self.USER_PASSWORD,
                                          host=self.HOST,
                                          port=self.PORT,
                                          database=self.DB_NAME)

            cursor = connection.cursor()
            cursor.execute(query)
            result = cursor.fetchall()
            table_column_name = cursor.description

        except (Exception, psycopg2.Error) as error:
            raise ("Error while connecting to PostgreSQL", error)

        finally:
            if connection:
                cursor.close()
                connection.close()

            return table_column_name, result, True

    def _run_query(self, query):
        """
        Runs a query upon given query string.
        :param query: Query String for Cursor
        :return: A dict if query is s
        """
        table_column_name, response, boolean_value = self.__setup_db(query)
        if boolean_value:
            return {
                'table_column_list': table_column_name,
                'table_data_list': response
            }
        else:
            return False
