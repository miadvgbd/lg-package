import re
import requests

from lg_pack.base.ini_file_service import _parse_ini_file
from lg_pack import config

PROTOCOL = config.PROTOCOL or _parse_ini_file(section='AUTHENTICATION', key='PROTOCOL')
ENDPOINT = config.ENDPOINT or _parse_ini_file(section='AUTHENTICATION', key='ENDPOINT')


class LgBaseAPI:
    """
    A Parent Util Class for all the CRM API
    """
    def __init__(self, auth_token=None):
        if auth_token:
            self.AUTH_TOKEN = auth_token
        else:
            try:
                token = _parse_ini_file(section='AUTHENTICATION', key='LG_AUTH_TOKEN')
            except:
                raise ValueError("Authentication token is missing")

            self.AUTH_TOKEN = token

        if PROTOCOL == 'https':
            self.PROTOCOL = PROTOCOL
        else:
            raise ValueError("Please use https Protocol")

        if ENDPOINT:
            self.ENDPOINT = ENDPOINT
        else:
            raise ValueError("API ENDPOINT missing")

    def __get_header(self):
        """
        Method for manipulating API Header

        :return: authentication token
        """
        return {'Authorization': self.AUTH_TOKEN}

    def _get_endpoint(self):
        """
        Method for formatting API Endpoint

        :return: URL (API endpoint with https)
        """
        return '{protocol}://{endpoint}//'.format(
            protocol=self.PROTOCOL,
            endpoint=self.ENDPOINT,
        )

    def _get(self, url, case_id=None, payload=None):
        """
        Base/Main GET method for performing GET requests.

        :param url: An api endpoint/url
        :param case_id: (Integer). CaseID of a CRM User.
        Formatted value and otherwise a dictionary.

        :return: JSON formatted values.
        """
        lg_api_endpoint = self._get_endpoint()
        api_url = lg_api_endpoint + url
        header = self.__get_header()
        params = {'CaseID': case_id} if case_id else payload

        api_response = requests.get(
            url=api_url,
            headers=header,
            params=params
        )

        if api_response.status_code == 200:
            response = {
                "status": "Successful",
                "data": api_response.json()
            }
        else:
            response = {
                "status": "Failed",
                "reason": str(api_response.status_code) + ' from CRM API',
                }

        return response

    def _post(self, url, body=None, file=None, case_id=None, params=None):
        """
        Base/Main POST method for performing POST requests.

        :param url: An api endpoint/url
        :param body: (Optional) API Request Body containing data parameters in Dictionary Format
        :param file: (Optional) Location of the File to be sent
        :param case_id: (Optional/Integer). CaseID of a CRM User.

        :return: Return a dictionary with 'status'of the request and 'data' contained with it
        """
        lg_api_endpoint = self._get_endpoint()
        api_url = lg_api_endpoint + url
        header = self.__get_header()
        params = {'CaseID': case_id} if case_id else params
        files = {'file': open(file, 'rb')} if file else None

        api_response = requests.post(
            url=api_url,
            headers=header,
            json=body,
            params=params,
            files=files
        )

        if api_response.status_code == 200:
            try:
                message = {
                    "status": "Successful",
                    "data": api_response.json()
                }
            except ValueError:
                message = self._lead_gen_response(
                    text=api_response.text
                )

            return message

        else:
            message = {
                "status": "Failed",
                "reason": str(api_response.status_code) + ' from CRM API. ' + str(api_response.text),
                }
            return message

    def _delete(self, url, payload):
        """
        Base/Main Delete method for performing DELETE requests.

        :param url: An api endpoint/url
        :param payload: Request Parameters

        :return: JSON formatted values.
        """
        lg_api_endpoint = self._get_endpoint()
        api_url = lg_api_endpoint + url
        header = self.__get_header()
        params = payload

        api_response = requests.delete(
            url=api_url,
            headers=header,
            params=params
        )

        if api_response.status_code == 200:
            response = {
                "status": "Successful",
                "data": api_response.json()
            }
        else:
            response = {
                "status": "Failed",
                "reason": str(api_response.status_code) + ' from CRM API',
                }

        return response

    def _update_case(self, url, params):
        """
        Base/Main POST method for performing Update in Case Update Request.
        :param url: An api endpoint/url

        :return: Return a dictionary with 'status'of the request and 'data' contained with it
        """
        lg_api_endpoint = self._get_endpoint()
        api_url = lg_api_endpoint + url

        api_response = requests.post(
            url=api_url,
            params=params
        )

        if api_response.status_code == 200:
            message = {
                "status": "Successful",
                "data": api_response.text
            }
            return message

        else:
            message = {
                "status": "Failed",
                "reason": str(api_response.status_code) + ' from CRM API. ' + str(api_response.text),
                }
            return message

    @staticmethod
    def _lead_gen_response(text):
        if re.search('[0-9][0-9][0-9][0-9][0-9][0-9]', text):
            re_data = re.findall('[0-9][0-9][0-9][0-9][0-9][0-9]', text)[0]
            message = {
                "status": "Successful",
                "data": {'CaseID': re_data}
            }
        elif re.search('[0-9][0-9][0-9][0-9][0-9][0-9][0-9]', text):
            re_data = re.findall('[0-9][0-9][0-9][0-9][0-9][0-9][0-9]', text)[0]
            message = {
                "status": "Successful",
                "data": {'CaseID': re_data}
            }
        elif re.search('Lead post failed due to duplicate entry', text):
            message = {
                "status": "Failed",
                "reason": 'Lead post failed due to duplicate entry'
            }
        elif re.search('Invalid lead', text):
            message = {
                "status": "Failed",
                "reason": 'Invalid lead'
            }
        else:
            message = {
                "status": "Failed",
                "reason": 'Try to POST Lead with Valid format'
            }

        return message
