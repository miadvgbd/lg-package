import os
from configparser import ConfigParser

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
CONFIG_FILE_PATH = os.path.join(BASE_DIR, 'lg_pack', 'config.py')
INI_FILE_PATH = os.path.join(BASE_DIR, 'lg_pack', 'conf.ini')


def _get_ini_file():
    print("Please enter your AUTHENTICATION TOKEN of CRM")
    print("----------------------------------------------")
    token = input()
    print("Please enter your API Endpoint")
    endpoint = input()
    print("\nPlease enter your DATABASE Credentials")
    print("--------------------------------------")
    print("Database Name")
    db_name = input()
    print("Database User Name")
    db_user = input()
    print("Database Password")
    db_pass = input()
    print("Database Host Address")
    db_host = input()
    print("Database Port")
    db_port = input()

    with open(INI_FILE_PATH, 'w+') as ini_file:
        conf_string = '[AUTHENTICATION]\nLG_AUTH_TOKEN = {token}\n' \
                      'ENDPOINT = {endpoint}\nPROTOCOL = https\n[API_END_POINT]\n'\
                    .format(
                            token=token,
                            endpoint=endpoint
                            )
        db_string = '[DB_CONFIGS]\nDB_NAME = {db_name}\nUSER_NAME = {db_user}\nUSER_PASSWORD = {db_pass}\n' \
                    'HOST = {db_host}\nPORT = {db_port}'.format(
                            db_name=db_name,
                            db_user=db_user,
                            db_pass=db_pass,
                            db_host=db_host,
                            db_port=db_port
                        )
        ini_file.writelines(conf_string)
        with open(CONFIG_FILE_PATH, 'r') as config_file:
            line_list = config_file.readlines()
            for line_number, lines in enumerate(line_list):
                if line_number > 2 and line_number < 14 :
                    ini_file.writelines(str(lines))
            ini_file.write(db_string)

    return 'Configuration for CRM has been set up successfully'


def _parse_ini_file(section=None, key=None):
    config = ConfigParser()
    config.read(INI_FILE_PATH)
    if section and key:
        if section == "API_END_POINT":
            value = config.get(str(section), str(key))
            stripped_value = value.strip('''\'''')
            return stripped_value
        return config.get(str(section), str(key))
    return None
