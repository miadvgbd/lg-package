import json

from lg_pack import config
from lg_pack.service.constant import LEAD_FIELDS, MANDATORY_FIELDS
from lg_pack.base.base_service import LgBaseAPI
from lg_pack.base.ini_file_service import _parse_ini_file

LEAD_GEN_API_URL = config.LEAD_GEN_API_URL or _parse_ini_file(section='API_END_POINT', key='LEAD_GEN_API_URL')


class LeadGen(LgBaseAPI):
    """
    Class consists of all the API Responses of LEAD Generation related APIs in CRM.
    """
    def create_lead(self, json_format=False, **kwargs):
        """Creates a lead in the CRM.

        Parameters
        ----------
        json_format : bool, optional
            If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary.

        **kwargs
            Arbitrary keyword arguments.
            
        Other Parameters
        ----------------
        FNAME : `string`, required
        LNAME : `string`, optional
        LEAD_PROVIDER_ID : `string`, optional
            Unique ID/Name for Lead generator.
            |br| **NOTES** |br|
            This field is not required, but it helps to identify the lead source in CRM. If not sent, system uses a
            general "Internet" lead source. The name used in this field should match with the name of its corresponding
            lead source in the CRM.
        PRODUCTID : `string`, optional
            Unique ID for Product.
            |br| **NOTES** |br|
            This Field is not required for posting into IRSLogics. For posting into Student Logics the field is required
            and the value should be 7 And for posting into Debt Collection, pass the value 12.
        CHECKDUPLICATES : `string`, optional
            Checking for duplicate leads.
            |br| **NOTES** |br|
            This Field is not required for posting into the CRM. For checking duplication of the lead posted this field
            can be used. any fields passed can be check for duplication by passing as parameter in this field seperated
            by a comma(,)
            |br| Example: FNAME, LNAME, SSN, ZIP
        ADDRESS : `string`, optional
        CITY : `string`, optional
        ZIP : `string`, optional
        STATE : `string`, optional
        EMAIL : `string`, optional
        HOME_PHONE : `string`, optional
            |br| Format: (NNN)NNN-NNNN
        WORK_PHONE : `string`, optional
            |br| Format: (NNN)NNN-NNNN
        CELL_PHONE : `string`, optional
            |br| Format: (NNN)NNN-NNNN
        DOB : `string`, optional
            |br| Format: MM/dd/yyyy
        SSN : `string`, optional
            |br| Format: NNN-NN-NNNN
        MARITAL_STATUS : `string`, optional
            |br| NOTE |br|
            Accepted values:
            |br| "Single" / "Married Filing Jointly" / "Married Filing Separately"
        EMPLOYMENT_TYPE : `string`, optional
            |br| NOTE |br|
            Accepted values:
            |br| "W-2" / "1099" / "W-2 , 1099" / "Unemployed" / "Retired" / "Disabled"
        TYPE_OF_WORK : `string`, optional
            |br| NOTE |br|
            Max 50 Characters.
        LANGUAGE : `string`, optional
            |br| NOTE |br|
            Accepted values:
            |br| "English" / "Spanish"
        AptNo : `string`, optional
        BEST_TIME_TO_CALL : `string`, optional
        SPOUSE_FIRSTNAME : `string`, optional
        SPOUSE_MIDDLENAME : `string`, optional
        SPOUSE_LASTNAME : `string`, optional
        SPOUSE_EMAIL : `string`, optional
        SPOUSE_HOME_PHONE : `string`, optional
            |br| Format: (NNN)NNN-NNNN
        SPOUSE_WORK_PHONE : `string`, optional
            |br| Format: (NNN)NNN-NNNN
        SPOUSE_CELL_PHONE : `string`, optional
            |br| Format: (NNN)NNN-NNNN
        SPOUSE_DOB : `string`, optional
            |br| Format: MM/dd/yyyy
        SPOUSE_SSN : `string`, optional
            |br| Format: NNN-NN-NNNN
        TAX_RELIEF_TAX_AMOUNT : `string`, optional, Total amount owed to IRS and State.
            |br| NOTE |br|
            Up to 15 digits with a decimal point (no dollar symbol)
        TAX_RELIEF_TAX_AGENCY : `string`, optional, Tax Agency (Federal or State)
            |br| NOTE |br|
            Accepted values:
            |br| "FEDERAL" / "STATE" / "FEDERAL and STATE"
        TAX_RELIEF_TAX_TYPE : `string`, optional, Tax Type
            |br| NOTE |br|
            Accepted values:
            |br| "PERSONAL" / "BUSINESS" / "PERSONAL and BUSINESS" / "PAYROL" / "OTHER"
        TAX_RELIEF_PRIMARY_PROBLEM : `string`, optional, Primary Tax Problem
            |br| NOTE |br|
            Accepted values:
            |br| "ASSETS_SEIZED" / "BANK_ACCOUNT_LEVY" / "CANT_PAY_UNPAID_TAXES" / "INNOCENT_SPOUSE" / "LIEN_FILED" /
            "RECEIVED_AUDIT_NOTICE" / "UNFILED_TAX_RETURNS" / "UNPAID_PENALTIES_AND_INTEREST" / "WAGE_GARNISHMENT" / "OTHER"
        FORM_NUMBERS : `string`, optional, Forms numbers used
            |br| NOTE |br|
            Can contain one or many of these values:
            |br| "Federal 1040" / "Civil Penalty" / "Federal 1120" / "Federal 940/941"
        BUSINESS_NAME : `string`, optional
        BUSINESS_TYPE : `string`, optional, Business Ownership Type
            |br| NOTE |br|
            Can contain one of these values:
            |br| "Sole Proprietorship" / "Partnership" / "LLP" / "LLC (Single)" / "LLC (Multiple)" / "S Corp" / "C Corp"
        EMPLOYER_ID_NO : `string`, optional
        SETOFFICERNAME : `string`, optional, Settlement Officer Name[FirstName LastName]
        TEAMID : `string`, optional, Team/Affiliate ID
            |br| NOTE |br|
                Unique ID assigned to the team/affiliate.
        TEAM : `string`, optional, Team/Affiliate Name
            |br| NOTE |br|
                Use Team ID(preferred) or Team Name.
        OPENERNAME : `string`, optional
        SERVICE : `string`, optional, Primary Service
            |br| NOTE |br|
                Value should match with any of the Service Names in CRM
        NOTES : `string`, optional
        LOAN_AMOUNT : `decimal`, optional, Up to 15 digits with a decimal point (no dollar symbol)
        MONTHLY_PAYMENT : `decimal`, optional, Up to 15 digits with a decimal point (no dollar symbol)
        CLIENT_AGI : `decimal`, optional, Client's Income (AGI), Up to 15 digits with a decimal point (no dollar symbol)
        SPOUSE_AGI : `decimal`, optional, Spouse Income (AGI), Up to 15 digits with a decimal point (no dollar symbol)
        HOUSEHOLD_SIZE : `integer`, optional, Family Size
        PIN : `integer`, optional
        ORIGINAL_BALANCE : `decimal`, optional, Balance Amount, Up to 15 digits with a decimal point (no dollar symbol)
        PRINCIPAL : `decimal`, optional, Principal Amount, Up to 15 digits with a decimal point (no dollar symbol)
        ACCRUED_INTEREST : `decimal`, optional, Up to 15 digits with a decimal point (no dollar symbol)
        PAID_TO_DATE : `decimal`, optional, Up to 15 digits with a decimal point (no dollar symbol)
        INTEREST_RATE : `string`, optional
        LENDER : `string`, optional
        APPLICANT_ID : `string`, optional
        DATE_OF_LOAN : `string`, optional
            |br| Format: MM/dd/yyyy
        COLLECTION_DATE : `string`, optional
            |br| Format: MM/dd/yyyy
        NSF_COUNT : `integer`, optional
        LOAN_TYPE : `string`, optional
            |br| NOTE |br|
            Can contain one of these values:
            |br| AUTO_LOANS / CONSUMER_LOANS / CREDIT_CARDS / MEDICAL / MORTAGES / OVER_DRAFT / PAY_DAY_LOAN /
             STUDENT_LOAN / TELECOMM / UTILITY / OTHER
        CHARGE_OFF_AMOUNT : `decimal`, optional, Up to 15 digits with a decimal point (no dollar symbol)
        CHARGE_OFF_DATE : `string`, optional
            |br| Format: MM/dd/yyyy
        LAST_PAID_AMOUNT : `decimal`, optional, Up to 15 digits with a decimal point (no dollar symbol)
        LAST_PAID_DATE : `string`, optional
            |br| Format: MM/dd/yyyy
        UDF1 : `string`, optional, Field Number 1 of Other fields(prefix should be "UDF")
            |br| NOTE |br|
            |br| (Integer, String, Date, Amount) Values can be any type, according to the fields type.
            1 = UDFID (we can get it from Administaration -> additional fields)
        UDF2 : `string`, optional, Field Number 2 of Other fields(prefix should be "UDF")
            |br| NOTE |br|
            |br| (Integer, String, Date, Amount) Values can be any type, according to the fields type.
            2 = UDFID (we can get it from Administaration -> additional fields)
        CrUserName : `string`, optional
        CrPassword : `string`, optional
        CreditReportCompany : `string`, optional
        SALESREPNAME : `string`, optional, Sales Rep Assigned

        Returns
        -------
        dict/json
            JSON/Dictionary upon the json_format parameter containing created CaseID from CRM.
        """
        params = {}
        if all(key in kwargs for key in MANDATORY_FIELDS):
            for _key, _val in kwargs.items():
                for field in LEAD_FIELDS:
                    if field == str(_key):
                        params[_key] = _val
            api_response = self._post(
                url=LEAD_GEN_API_URL,
                params=params
            )
        else:
            api_response = {
                "status": "Failed",
                "Reason": "Please give the Required Field"
            }

        if api_response['status'] == "Successful":
            response_dict = {
                "Status": "Successfully added a Lead",
                "CaseID": api_response['data']['CaseID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response
