import json

from lg_pack import config
from lg_pack.base.base_service import LgBaseAPI
from lg_pack.base.ini_file_service import _parse_ini_file

CASE_DOCUMENT_API_URL = config.CASE_DOCUMENT_API_URL or _parse_ini_file(section='API_END_POINT',
                                                                        key='CASE_DOCUMENT_API_URL')


class Document(LgBaseAPI):
    """
    Class consists of all the API Responses of Tasks/Events related APIs in CRM.
    """
    def create_case_document(self, case_id, file, json_format=False):
        """
        Uploads a file to a specific case as a Case Document.
        The maximum uploaded file size is 6 MB per file so it’s recommended to do compression/PDF optimization.
        This endpoint accepts files in multipart/form-data format

        :param file: Files in multipart/form-data format(e.g. PDF)
        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.
        """
        api_response = self._post(
            url=CASE_DOCUMENT_API_URL,
            case_id=case_id,
            file=file,
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "Status": "Successfully Updated Document",
                "CaseDocumentID": api_response['data']['data']['CaseDocumentID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response
