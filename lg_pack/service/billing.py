import json

from lg_pack import config
from lg_pack.base.base_service import LgBaseAPI
from lg_pack.base.ini_file_service import _parse_ini_file
from lg_pack.service.case import Case


CASE_ACCOUNT_API_URL = config.CASE_ACCOUNT_API_URL or _parse_ini_file(section='API_END_POINT',
                                                                      key='CASE_ACCOUNT_API_URL')
CASE_PAYMENT_API_URL = config.CASE_PAYMENT_API_URL or _parse_ini_file(section='API_END_POINT',
                                                                      key='CASE_PAYMENT_API_URL')
CASE_INVOICE_API_URL = config.CASE_INVOICE_API_URL or _parse_ini_file(section='API_END_POINT',
                                                                      key='CASE_INVOICE_API_URL')
CASE_AMORTIZATION_API_URL = config.CASE_AMORTIZATION_API_URL or _parse_ini_file(section='API_END_POINT',
                                                                                key='CASE_AMORTIZATION_API_URL')


class Billing(LgBaseAPI):
    """
    Class consists of all the API Responses of Billing/Payment related APIs in CRM
    """
    def create_case_account(self, request_body, json_format=False):
        """
        Create a case account (credit card or bank account) with default values

        Parameters
        ----------
        request_body : A dictionary
            All parameters are optional but accountType, (ccType,ccExpDate, ccNo, ccSecurityNo) OR
            (bankAccountNo, bankRoutingNo, nameOnAccount, bankName) should be exactly formatted like this:
            Dictionary Format:
                {
                    | "accountType": Integer,
                    | "primaryAccount": Boolean,
                    | "bankName": String,
                    | "bankRoutingNo": Integer,
                    | "bankAccountNo": Integer,
                    | "ccType": Integer,
                    | "ccNo": String | e.g. "4111111111111111",
                    | "ccExpDate": String | e.g. "022023",
                    | "ccSecurityNo": String | e.g. "123",
                    | "billingAddressSameAsCase": "false",
                    | "address": String | e.g. "401 N Michigan Ave",
                    | "aptNo": String | e.g. "Ste 1200",
                    | "city": String | e.g. "Chicago",
                    | "state": String | Short form of states | e.g. "IL",
                    | "zip": Integer | 60611,
                    | "nameOnAccount": String | e.g. "John Smith",
                    | "caseID": Integer,
                    | "phoneNo": String | Strict Format | e.g. "(312)914-7211",
                    | "emailID": String | e.g. "john@smith.com"
                }

        json_format : (Optional|Boolean| Default: False)
             If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter containing a CaseAccountID
        """
        api_response = self._post(
            url=CASE_ACCOUNT_API_URL,
            body=request_body
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": api_response['data']['message'] if api_response['data']['data'] is None
                else "Case Payment created successfully",
                "CaseAccountID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['CaseAccountID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def get_case_account(self, case_id, json_format=False):
        """
        Gets a list of case accounts for the provided CaseID.

        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON Formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.
        """
        api_response = self._get(
            url=CASE_ACCOUNT_API_URL,
            case_id=case_id
        )

        if api_response['status'] == "Successful":
            if json_format:
                response = api_response['data']['data']
            else:
                response = json.loads(api_response['data']['data'])
        else:
            response = api_response

        return response

    def create_case_payment(self, request_body, json_format=False):
        """
        Create a case payments with default values

        Parameters
        ----------
        request_body : A dictionary (Must have KEYS: {PaymentTypeID, CaseID, Amount})
            A dictionary containing all possible acceptable KEYS. Must have KEYS (PaymentTypeID, CaseID, Amount).
            Other KEYS are optional.
            |br| Should be exactly formatted like this:
            Dictionary Format:
                {
                    | "caseID": Integer,
                    | "paymentTypeID": Integer,
                    | "paidDate": Date Format | e.g. "2010-08-26",
                    | "amount": Float | e.g. 100.00,
                    | "comment": String | Optional
                }
                |br| NOTE:
                    paymentTypeID  ==> 1 (CreditCard) (will charge the Credit Card or Bank Account via default merchant
                                                      account provider)
                                   ==> 2 (Checking) (will charge the Credit Card or Bank Account via default merchant
                                                    account provider)
                                   ==> 3 (Cash) (will only post a record but will not run a payment)
                                   |br| ==> 4 (Refund)
                                   |br| ==> 5 (Offset)
                                   |br| ==> 6 (ChargeBack)
        json_format : (Optional|Boolean| Default: False)
             If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter containing created CasePaymentID.
        """
        api_response = self._post(
            url=CASE_PAYMENT_API_URL,
            body=request_body
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": True,
                "message": api_response['data']['message'] if api_response['data']['data'] is None
                else "Case Payment created successfully",
                "CasePaymentID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['CasePaymentID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def get_case_payment(self, case_id, json_format=False):
        """
        Gets a list of case payments for the provided CaseID.

        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON Formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.
        """
        api_response = self._get(
            url=CASE_PAYMENT_API_URL,
            case_id=case_id
        )

        if api_response['status'] == "Successful":
            if json_format:
                response = api_response['data']['data']
            else:
                response = json.loads(api_response['data']['data'])
        else:
            response = api_response

        return response

    def create_case_invoice(self, request_body, json_format=False):
        """
        Creates a case invoices with default values

        Parameters
        ----------
        request_body : A dictionary (Must have KEYS: {invoiceTypeID or invoiceTypeName, caseID, unitPrice, quantity})
            A dictionary containing all possible acceptable KEYS. Must have KEYS (invoiceTypeID or invoiceTypeName,
            caseID, unitPrice, quantity). Other KEYS are optional.
            |br| Should be exactly formatted like this:
            Dictionary Format:
                {
                  | "caseID": Integer,
                  | "date": Date | e.g. "2020-08-26",
                  | "invoiceTypeID": Integer,
                  | "invoiceTypeName": String | e.g. "Resolution Fee",
                  | "unitPrice": Integer,
                  | "quantity": Integer,
                  | "description": String | Optional
                }

        json_format : (Optional|Boolean| Default: False)
             If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter containing created CaseInvoiceID.
        """
        api_response = self._post(
            url=CASE_INVOICE_API_URL,
            body=request_body
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": api_response['data']['message'] if api_response['data']['data'] is None
                else "Case Payment created successfully",
                "CaseInvoiceID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['CaseInvoiceID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def get_case_invoice(self, case_id, json_format=False):
        """
        Gets a list of case invoices for the provided CaseID.

        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON Formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.
        """
        api_response = self._get(
            url=CASE_INVOICE_API_URL,
            case_id=case_id
        )

        if api_response['status'] == "Successful":
            if json_format:
                response = api_response['data']['data']
            else:
                response = json.loads(api_response['data']['data'])
        else:
            response = api_response

        return response

    def delete_case_invoice(self, case_invoice_id, json_format=False):
        """
        Delete a case invoices with default values.

        :param case_invoice_id: (integer)
        :param json_format: JSON/Dictionary upon the json_format parameter containing created CaseInvoiceID.
        :return: Returns a json containing deleted CaseInvoiceID
        """
        api_response = self._delete(
            url=CASE_INVOICE_API_URL,
            payload={
                'CaseInvoiceID': int(case_invoice_id)
            }
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": api_response['data']['message'] if api_response['data']['data'] is None
                else "Case Invoice Deleted successfully",
                "CaseInvoiceID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['CaseInvoiceID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def create_case_amortization(self, request_body, json_format=False):
        """
        Creates a case amortization (payment schedule) with default values

        Parameters
        ----------
        request_body : A dictionary
            Should be exactly formatted like this:
            Dictionary Format:
                {
                  | "caseID": Integer,
                  | "amount": Float,
                  | "scheduledDate": Date | e.g. "2020-08-26"
                }

        json_format : (Optional|Boolean| Default: False)
             If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter containing created CaseAmortizationID.
        """
        api_response = self._post(
            url=CASE_AMORTIZATION_API_URL,
            body=request_body
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": api_response['data']['message'] if api_response['data']['data'] is None
                else "Case Payment created successfully",
                "CaseAmortizationID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['CaseAmortizationID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def get_case_amortization(self, case_id, json_format=False):
        """
        Gets a list of case amortization for the provided CaseID.

        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON Formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.
        """
        api_response = self._get(
            url=CASE_AMORTIZATION_API_URL,
            case_id=case_id,
        )

        if api_response['status'] == "Successful":
            if json_format:
                response = api_response['data']['data']
            else:
                response = json.loads(api_response['data']['data'])
        else:
            response = api_response

        return response

    def delete_case_amortization(self, case_amortization_id, json_format=False):
        """
        Delete a case amortization with default values.

        :param case_amortization_id: (integer)
        :param json_format: JSON/Dictionary upon the json_format parameter containing created CaseAmortizationID.
        :return: Returns a json containing deleted CaseAmortizationID
        """
        api_response = self._delete(
            url=CASE_AMORTIZATION_API_URL,
            payload={
                'CaseAmortizationID': int(case_amortization_id)
            }
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": api_response['data']['message'] if api_response['data']['data'] is None
                else "Case amortization Deleted successfully",
                "CaseAmortizationID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['CaseAmortizationID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def add_card_and_payment(self, case_id, last4_digit, card_type, cc_number, cc_expire_date, cc_security_no):
        """
        Checks if a Payment card exists or adds a card if it is new.
        :param cc_security_no: Card Security Number
        :param cc_expire_date: Card Expiration date (Format: (MMYYDD) String | e.g.(122022))
        :param cc_number:
        :param card_type: Card Type (i.e. Visa/MasterCard/AMEX/Discover)
        :param case_id: CaseID of the CRM (Integer)
        :param last4_digit: Last 4 digits of the card (Integer)
        :return: dict with success status
        """
        case_response = self.get_case_account(
            case_id=case_id
        )

        is_new_card = True  # bool to check for new card
        for case in case_response:
            cc = case['CCNo'].replace(" ", "")
            cc = cc.replace("*", "")
            if cc == str(last4_digit):
                is_new_card = False  # card found
                break  # exit as found card

        # check card type and assign card code type according to the crm
        if card_type == 'Visa':
            card_type_code = '1'
        elif card_type == 'MasterCard':
            card_type_code = '2'
        elif card_type == 'AMEX':
            card_type_code = '3'
        elif card_type == 'Discover':
            card_type_code = '4'
        else:
            card_type_code = '0'

        # add payment card in the crm if it is new
        if is_new_card:
            case_body = {
                'caseID': case_id,
                'accountType': '2',
                'primaryAccount': 'true',
                'ccType': card_type_code,
                'ccNo': cc_number,
                'ccExpDate': f'{cc_expire_date[0:2]}20{cc_expire_date[2:]}',
                'ccSecurityNo': cc_security_no
                }
            card_response = self.create_case_account(
                request_body=case_body
            )

            # create an activity if a new card is used for payment
            activity_post_body = {
                'CaseID': case_id,
                'ActivityType': 'CardVerified',
                'Subject': 'Card Successfully Added and charged using fast payment link',
                'Comment': 'a {} card ended with \'{}\' is charged.'.format(card_type, last4_digit)
            }
            activity_response = Case(self.AUTH_TOKEN).create_activity(
                request_body=activity_post_body
            )
            if activity_response['status'] == 'Activity created successfully' and card_response['status']:
                return {
                    'status': True,
                    'msg': 'New card added & Activity created successfully'
                }
            return {
                'status': False,
                'msg': 'New card addition failed. Please check card details'
            }

        else:
            # create an activity of the new payment with the existing card
            activity_post_body = {
                'CaseID': case_id,
                'ActivityType': 'CardVerified',
                'Subject': 'Vyapay Payment Success using fast payment link',
                'Comment': 'a {} card ended with \'{}\' is charged.'.format(card_type, last4_digit)
            }
            activity_response = Case(self.AUTH_TOKEN).create_activity(
                request_body=activity_post_body
            )
            if activity_response['status'] == 'Activity created successfully':
                return {
                    'status': True,
                    'msg': 'Existing card verified & Activity created successfully'
                    }
            return {
                    'status': False,
                    'msg': 'Card verification failed. Please check card details'
                }

    def add_card(self, case_id, last4_digit, card_type, cc_number, cc_expire_date, cc_security_no):
        """
        Checks if a Payment card exists or adds a card if it is new.
        :param cc_security_no: Card Security Number
        :param cc_expire_date: Card Expiration date (Format: (MMYYDD) String | e.g.(122022))
        :param cc_number:
        :param card_type: Card Type (i.e. Visa/MasterCard/AMEX/Discover)
        :param case_id: CaseID of the CRM (Integer)
        :param last4_digit: Last 4 digits of the card (Integer)
        :return: dict with success status
        """
        case_response = self.get_case_account(
            case_id=case_id
        )

        is_new_card = True  # bool to check for new card
        for case in case_response:
            cc = case['CCNo'].replace(" ", "")
            cc = cc.replace("*", "")
            if cc == str(last4_digit):
                is_new_card = False  # card found
                break  # exit as found card

        # check card type and assign card code type according to the crm
        if card_type == 'Visa':
            card_type_code = '1'
        elif card_type == 'MasterCard':
            card_type_code = '2'
        elif card_type == 'AMEX':
            card_type_code = '3'
        elif card_type == 'Discover':
            card_type_code = '4'
        else:
            card_type_code = '0'

        # add payment card in the crm if it is new
        if is_new_card:
            case_body = {
                'caseID': case_id,
                'accountType': '2',
                'primaryAccount': 'true',
                'ccType': card_type_code,
                'ccNo': cc_number,
                'ccExpDate': f'{cc_expire_date[0:2]}20{cc_expire_date[2:]}',
                'ccSecurityNo': cc_security_no
                }
            card_response = self.create_case_account(
                request_body=case_body
            )
            if card_response['status']:
                return {
                    'status': True,
                    'new_card': True,
                    'msg': 'New card added successfully'
                }
            return {
                'status': False,
                'msg': 'New card addition failed. Please check card details'
            }

        else:
            return {
                'status': True,
                'new_card': False,
                'msg': 'Existing card verified successfully'
                }
