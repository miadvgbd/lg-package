import json

from lg_pack import config
from lg_pack.base.base_service import LgBaseAPI
from lg_pack.base.ini_file_service import _parse_ini_file

CASE_API_URL = config.CASE_API_URL or _parse_ini_file(section='API_END_POINT', key='CASE_API_URL')
ACTIVITY_API_URL = config.ACTIVITY_API_URL or _parse_ini_file(section='API_END_POINT', key='ACTIVITY_API_URL')
GET_CASEID_API_URL = config.GET_CASEID_API_URL or _parse_ini_file(section='API_END_POINT', key='GET_CASEID_API_URL')
UPDATE_CASE_API_URL = config.UPDATE_CASE_API_URL or _parse_ini_file(section='API_END_POINT', key='UPDATE_CASE_API_URL')


class Case(LgBaseAPI):
    """
    Class consists of all the API Responses of Case related APIs in CRM
    """
    def get_case(self, case_id=None, details=None, json_format=False):
        """
        Gets a list of case properties for the provided CaseID

        :param details: (Dict) Parameters to get from the CaseID in a dictionary format
        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.LG_AUTH_TOKEN
        """
        if details:
            api_response = self._get(
                url=CASE_API_URL,
                payload=details
            )
        else:
            api_response = self._get(
                url=CASE_API_URL,
                case_id=case_id
            )

        if api_response['status'] == "Successful":
            if json_format:
                response = api_response['data']['data']
            else:
                response = json.loads(api_response['data']['data'])
        else:
            response = api_response

        return response

    def create_case(self, request_body, json_format=False):
        """
        Creates a case with default values

        Parameters
        ----------
        request_body : A dictionary ( Must have KEYS: {CellPhone, SSN, State})
            A dictionary containing all possible acceptable KEYS. Must have KEYS (CellPhone, SSN, State). Other KEYS are optional.
            |br| Must be exactly formatted like this:
            {
                | "StatusID": Integer,
                | "ProductID": Integer,
                | "FirstName": String,
                | "LastName" : String,
                | "CellPhone": String | Format: "(312)914-7211",
                | "WorkPhone": String,
                | "HomePhone" : String,
                | "Email": String | e.g.: "john@smith.com",
                | "City": String,
                | "State" : String | Short form of STATES | e.g: "IL",
                | "Zip"  : String | e.g: "60601",
                | "Address": String | e.g: "401 N Michigan Ave",
                | "AptNo": String,
                | "SSN": String | Format: "123-45-6789"
            }
        json_format : (Optional|Boolean| Default: False)
             If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter
        """
        api_response = self._post(
            url=CASE_API_URL,
            body=request_body,
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "Status": "Successfully Create a Case",
                "CaseID": api_response['data']['data']['CaseID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def update_case(self, case_id, json_format=False, **kwargs):
        """
        Update the parts of Case Data of the CRM.

        Parameters
        ----------
        case_id : integer
            CaseID of the User of the CRM.

        json_format : bool, optional
            If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary.

        **kwargs
            Arbitrary keyword arguments.

        Other Parameters
        ----------------
        soid : integer, Settlement Officer ID
        cmid : integer, Case Manager ID
        cwid : integer, Case Worker ID
        atid : integer, Attorney ID
        opid : integer, Opener ID
        statusid : integer, Status ID
        teamid : integer, Team ID
        fname : string, First Name
        lname : string, Last Name
        taxamount : float, Tax Amount
        distributionid : integer, Distribution template ID for assignment based on a distribution template
        aptno : string, Apartment Number
        address : string, Street Address
        state : string, State
        city : string, City
        zip : integer, Zip Code
        ssn : string, SSN, Format: xxx-xx-xxxx
        dob : string, Date of Birth, Format: mm/dd/yyyy
        homephone : string, Home Phone Number, Format: (xxx)xxx-xxxx
        workphone : string, Work Phone Number, Format: (xxx)xxx-xxxx
        cellphone : string, Cell Phone Number, Format: (xxx)xxx-xxxx
        sourceid : Integer, Lead Provider ID
        email : string, Email
        BusinessName : string, Business Name
        BusinessAddress : string, Business Address
        BusinessAptNo : string, Business Apartment Number
        BusinessCity : string, Business City
        BusinessState : string, Business State
        BusinessZip : integer, Zip Code of Business Location
        BusinessType : integer
            Options:
               |br|  0 ==> N/A
               |br|  1 ==> Sole Proprietorship
               |br|  2 ==> Partnership
               |br|  3 ==> LLP
               |br|  4 ==> LLC (single)
               |br|  5 ==> LLC (multiple)
               |br|  6 ==> S Corp
               |br|  7 ==> C Corp
        BusinessEIN : string, Business EIN number , Format: (xx-xxxxxxx)
        sfname : string, Spouse First Name
        slname : string, Spouse Last Name
        sdob : string, Spouse Date of Birth
        semail : string, Spouse Email
        sssn : string, Spouse SSN
        UDFxxx: UDF fields can be set with the UDFxxx parameter name where xxx stands for UDF id.
        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter with the CaseID that has been UPDATED
        """
        params_body = {
            'CaseID': case_id,
            'apikey': self.AUTH_TOKEN
        }

        for _key, _val in kwargs.items():
            params_body[_key] = _val

        api_response = self._update_case(
            url=UPDATE_CASE_API_URL,
            params=params_body
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "Status": "Successfully Updated a Case",
                "CaseID": case_id
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def get_activity(self, case_id, json_format=False):
        """
        Gets all activities on a case.

        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON Formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.
        """
        api_response = self._get(
            url=ACTIVITY_API_URL,
            case_id=case_id
        )

        if api_response['status'] == "Successful":
            if json_format:
                response = json.dumps(api_response['data'])
            else:
                response = api_response['data']
        else:
            response = api_response

        return response

    def create_activity(self, request_body, json_format=False):
        """
        Creates an activity on the specified case

        Parameters
        ----------
        request_body : A dictionary
            Dictionary Format:
                {
                    | "CaseID": String | e.g.: "60815",
                    | "ActivityType": String | e.g.: "General",
                    | "Subject": String,
                    | "Comment": String | Optional | e.g.: "Called IRS"
                }
        json_format : (Optional|Boolean| Default: False)
             If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter
        """
        api_response = self._post(
            url=ACTIVITY_API_URL,
            body=request_body
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": "Activity created successfully"
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def get_case_id(self, json_format=False, **kwargs):
        """
        Get CaseID from CRM with with the Searching Parameters

        Parameters
        ----------
        json_format : bool, optional
            If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary.

        **kwargs
            Arbitrary keyword arguments.

        Other Parameters
        ----------------
        PhoneNumber : `string`

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter
        """
        payload = {}
        for _key, _val in kwargs.items():
            payload[_key] = _val

        api_response = self._get(
            url=GET_CASEID_API_URL,
            payload=payload
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "Status": "Successfully Retrieved a Case ID",
                "CaseID": api_response['data']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response
