import json

from lg_pack import config
from lg_pack.base.base_service import LgBaseAPI
from lg_pack.base.ini_file_service import _parse_ini_file

TASK_EVENT_API_URL = config.TASK_EVENT_API_URL or _parse_ini_file(section='API_END_POINT', key='TASK_EVENT_API_URL')


class TaskEvent(LgBaseAPI):
    """
    Class consists of all the API Responses of Tasks/Events related APIs in CRM.
    """
    def create_task_or_event(self, request_body, json_format=False):
        """
        Create a Task/Event  with default values.
        |br| DEF: A Task only has only a Due Date but for Event we need to pass EndDate too.
        |br| All Date times should be passed as UTC datetime.

        Parameters
        ----------
        request_body : A dictionary (Must have KEYS: {CaseID, PriorityID, StatusID, Reminder, TaskType, TaskCategoryID})
            |br| Note : DueDate is mandatory for Creating a Task.
                   DueDate and EndDate are mandatory both are for creating an Event.
            |br| A dictionary containing all possible acceptable KEYS. Must have KEYS (CaseID, PriorityID, StatusID, Reminder, TaskType, TaskCategoryID). Other KEYS are optional.
            |br| Should be exactly formatted like this:
            {
                | "dueDate": DateTime Format | e.g."2010-11-12 00:00:00.000",
                | "priorityID": Integer,
                | "caseID": Integer,
                | "statusID": Integer,
                | "subject": String,
                | "comments": String,
                | "reminder": DateTime Format | e.g."2010-11-11 16:00:00.000",
                | "taskType": Integer,
                | "endDate": DateTime Format | e.g."2015-04-18 03:30:00.000",
                | "allDayEvent": null,
                | "taskCategoryID": Integer,
                | "UserID": List | List of UserID’s of task assignees | e.g. [ 1,2,3,4,5,6,7,8,9,10]
            }
            |br| NOTE:
                statusID ==> 0 (Mark Complete)
                         ==> 1 (Mark Incomplete)

                |br| priorityID ==> 0 (Normal)
                           ==> 1 (Medium)
                           ==> 2 (High)
                           ==> 3 (Urgent)

                |br| taskType ==> 1 (Task)
                         ==> 2 (Event)
        json_format : (Optional | Boolean | Default: False)
             If this parameter is set to True, function will return a JSON formatted value and otherwise a dictionary

        Returns
        -------
        JSON/Dictionary
            JSON/Dictionary upon the json_format parameter containing created TaskID.
        """
        mandatory_keys = {
            "caseID", "priorityID", "statusID", "reminder", "taskType", "taskCategoryID"
        }

        if all(key in request_body for key in mandatory_keys):
            if request_body['taskType'] == 2:
                if request_body['dueDate'] and request_body['endDate']:
                    api_response = self._post(
                        url=TASK_EVENT_API_URL,
                        body=request_body,
                    )
                else:
                    api_response = {
                        "status": "Failed",
                        "reason": "Please Provide both DueDate and EndDate in request body to create an Event"
                    }
            else:
                if request_body['dueDate']:
                    api_response = self._post(
                        url=TASK_EVENT_API_URL,
                        body=request_body,
                    )
                else:
                    api_response = {
                        "status": "Failed",
                        "reason": "Please Provide DueDate in request body to create a Task"
                    }
        else:
            api_response = {
                "status": "Failed",
                "reason": "Please Provide the mandatory keys in request body to create a Task/Event."
                          "One or more of the mandatory fields from here {caseID, priorityID, statusID, reminder, taskType, taskCategoryID} are missing"

            }

        if api_response['status'] == "Successful":
            response_dict = {
                "status": api_response['data']['message'] if api_response['data']['data'] is None
                else "Case Payment created successfully",
                "TaskID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['TaskID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response

    def get_task_event(self, case_id, json_format=False):
        """
        Gets a list of Tasks/Events for the provided CaseID.

        :param case_id: (Integer). CaseID of a CRM User.
        :param json_format: (Boolean| Default: False). If this parameter is set to True, function will return a JSON Formatted value and otherwise a dictionary.

        :return: JSON/Dictionary upon the json_format parameter.
        """
        api_response = self._get(
            url=TASK_EVENT_API_URL,
            case_id=case_id
        )

        if api_response['status'] == "Successful":
            if json_format:
                response = api_response['data']['data']
            else:
                response = json.loads(api_response['data']['data'])
        else:
            response = api_response

        return response

    def delete_task_event(self, task_id, json_format=False):
        """
        Delete a task or event with default values.

        :param task_id: (integer)
        :param json_format: JSON/Dictionary upon the json_format parameter containing created TaskID.
        :return: Returns a json containing deleted TaskID
        """
        api_response = self._delete(
            url=TASK_EVENT_API_URL,
            payload={
                'TaskID': int(task_id)
            }
        )

        if api_response['status'] == "Successful":
            response_dict = {
                "status": api_response['data']['message'] if api_response['data']['data'] is None
                else "Task/Event Deleted successfully",
                "TaskID": None if api_response['data']['data'] is None
                else json.loads(api_response['data']['data'])['TaskID']
            }
            if json_format:
                response = json.dumps(response_dict)
            else:
                response = response_dict
        else:
            response = api_response

        return response
